#include<iostream>

using namespace std;

struct MyStruct
{
	int val = 10;
} myS1;

int operator+(int a, const MyStruct& b)
{
	return a-b.val;
}

int operator+(const MyStruct& b, int a)
{
	return b.val - a;
}


int main()
{
	int a=25;
	//int b=5;
	cout << myS1+a << endl;
	cout << a+myS1 << endl;

	return 0;
}